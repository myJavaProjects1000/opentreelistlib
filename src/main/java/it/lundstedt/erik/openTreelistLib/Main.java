package it.lundstedt.erik.openTreelistLib;
import it.lundstedt.erik.*;
import it.lundstedt.erik.openTreelistLib.tree.Child;
import it.lundstedt.erik.openTreelistLib.tree.Head;
import it.lundstedt.erik.openTreelistLib.tree.Node;
import it.lundstedt.erik.openTreelistLib.tree.Tree;

public class Main
{
	public static void main(String[] args) {

		Head head=new Head("im the head");
		Child branch=new Child("im a branch",head);
		Child todo=new Child("remember to eat",branch);
		Child branch2=new Child("im a branch",head);
		Child subBranch=new Child("im a sub branch",branch);
		Child subBranch1=new Child("im a subBranch",branch);
		Child subSubBranch=new Child("im a subSub branch",subBranch1);
		Child subSubBranch1=new Child("im a subSub Branch",subBranch1);
		
		Node[] tree=new Node[]{head,branch,todo,branch2,subBranch,subBranch1,subSubBranch,subSubBranch1};
		for (int i = 0; i <tree.length ; i++) {
			System.out.println(tree[i].getText());
		}
		System.out.println("*******************************************");
		Tree firstTree=new Tree(tree);
		firstTree.drawTree();
		System.out.println("*******************************************");
		firstTree.drawKids(2);
		System.out.println("*******************************************");
		firstTree.drawParents(2);
		System.out.println("*******************************************");
		System.out.println(firstTree.search("Branch"));
		System.out.println("*******************************************");
		System.out.println(firstTree.regexSearch("[a-z]+Branch"));
		System.out.println("*******************************************");
	}

}
