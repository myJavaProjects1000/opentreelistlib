package it.lundstedt.erik.openTreelistLib.tree;

public class Node
{
	int indent;
	
	String text;
	String branch="├──";
	
	public void setIndent(int indent) {
		this.indent = indent;
	}
	
	public String getBranch() {
		return branch;
	}
	
	public void setBranch(String branch) {
		this.branch = branch;
	}
	
	public Node(int indent, String text) {
		this.indent = indent;
		this.text = text;
	}
	
	public int getIndent() {
		return indent;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	
	//example implementation. override
	public void drawNode()
	{
	for (int i = 0; i <= indent; i++) {
		System.out.print("|	 ");
	}
	System.out.println(text);
	}
}


