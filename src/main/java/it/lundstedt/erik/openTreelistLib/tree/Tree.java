package it.lundstedt.erik.openTreelistLib.tree;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class Tree
{
Node[] tree;
	
	public Tree(Node[] nodes)
	{
		this.tree = nodes;
	}
	
	public void drawTree (){
		for (int i = 0; i <tree.length ; i++) {
			tree[i].drawNode();
		}
	}
	public void drawKids(int x)
	{
		
		for (int i = 0; i <tree.length ; i++) {
			if (tree[i].getIndent()>=x) {
				tree[i].drawNode();
			}
		}
	}
	public void drawParents(int x)
	{
		
		for (int i = 0; i <tree.length ; i++) {
			if (tree[i].getIndent()<=x) {
				tree[i].drawNode();
			}
		}
	}
	public String search (String term)
	{
		String result="";
		for (int i = 0; i <tree.length ; i++) {
			String test = tree[i].getText();
			if (test == term) result += ": " + test;
			else if (test.contains(term)) result += ": " + test;
		}
		return result;
	}
	
	// TODO: 2020-02-25 make this work 
	public String regexSearch (String regex)
	{
		Pattern r;
		r = Pattern.compile(regex);
		Matcher m;
		String result="no match";
		for (int i = 0; i <tree.length ; i++){
			String test = tree[i].getText();
			m = r.matcher(test);
			if (m.matches()){
				result+=": "+test;
			}
		}
		return result;
	}
	
	//else if (test.matches(term))result+=": "+test;
	
}
