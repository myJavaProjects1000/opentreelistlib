package it.lundstedt.erik.openTreelistLib.tree;

public class Child extends Node
{
	
	

	
	
	Node parent;
	
	public Child(String text,Node nodeParent) {
		super(nodeParent.indent+1, text);
		this.parent=nodeParent;
		
	}
	
	@Override
		public void drawNode()
		{
			
			for (int i = 1; i <=this.indent ; i++) {
				System.out.print("│	 ");
			}
			System.out.print(parent.branch);
			System.out.println(text+" "+this.indent);
		}
	
}
