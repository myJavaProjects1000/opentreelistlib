package it.lundstedt.erik.openTreelistLib.tree;

public class Head extends Node
{
	public Head(String text) {
		super(0, text);
	}
	
	@Override
	public void drawNode() {
		System.out.println(text);
	}
}
